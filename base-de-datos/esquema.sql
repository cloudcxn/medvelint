CREATE TABLE carga (
  ancho_banda INT,
  bytes INT,
  transcurrido INT
);

CREATE TABLE descarga (
  ancho_banda INT,
  bytes INT,
  transcurrido INT
);

CREATE TABLE interfaz (
  es_vpn VARCHAR(5),
  ip_externa VARCHAR(15),
  ip_interna VARCHAR(15),
  mac CHAR(17),
  nombre VARCHAR(8)
);

CREATE TABLE medicion (
  fecha VARCHAR(20),
  isp VARCHAR(32),
  tipo VARCHAR(16)
);

CREATE TABLE ping (
  fluctuacion FLOAT,
  latencia FLOAT
);

CREATE TABLE resultado (
  id VARCHAR(36),
  url VARCHAR(71)
);

CREATE TABLE servidor (
  huesped VARCHAR(32),
  id INT,
  ip VARCHAR(15),
  nombre VARCHAR(32),
  pais VARCHAR(32),
  puerto INT,
  ubicacion VARCHAR(32)
);
