# Librería con las funciones usadas por el medidor de velocidad de internet

# Recibe la ID del servidor y devuelve la medición como JSON
function ejecutar_medicion() {
  echo $(speedtest -f json -s $1)
}
