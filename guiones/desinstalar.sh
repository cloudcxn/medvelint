#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "Este guión debe ser ejecutado como raíz"
   exit 1
fi

directorio_instalacion="/opt/medvelint"

# Borrar el directorio en donde se instaló la aplicación
rm -r $directorio_instalacion/

# Remover enlace simbólico
rm /usr/local/bin/medvelint

# Remover el trabajo cron
rm /etc/cron.hourly/medvelint

# Terminar
echo "Desinstalación completa"
