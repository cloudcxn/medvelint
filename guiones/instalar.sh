#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "Este guión debe ser ejecutado como raíz. Saliendo del instalador."
   exit 1
fi

directorio_instalacion="/opt/medvelint"

# Crear el directorio en donde se instalará la aplicación
mkdir -p $directorio_instalacion/

# Establecer el directorio del guión de instalación
directorio_guion=$(dirname "$0")
cd $directorio_guion && cd ..

# Copiar los archivos PHP
cp codigo/* $directorio_instalacion/

# Copiar las librerias
cp lib/* $directorio_instalacion/

# Copiar los ejecutables
cp guiones/medvelint.sh $directorio_instalacion/

# Crear la base de datos
sqlite3 -init base-de-datos/esquema.sql \
  $directorio_instalacion/velocidad_internet.db | printf ".exit\n"

# Crear enlace simbólico
ln -s $directorio_instalacion/medvelint.sh /usr/local/bin/medvelint

# Crear trabajo cron cada hora
cp guiones/cron.sh /etc/cron.hourly/medvelint

# Terminar
echo "Instalación completa"
