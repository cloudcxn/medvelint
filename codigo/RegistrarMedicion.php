<?php
require 'autoload.php';

$vel_int = new VelocidadInternet($argv[1]);

// Crear un objeto de datos PHP para el manejo de la base de datos
$pdo = new PDO('sqlite:/opt/medvelint/velocidad_internet.db');

$carga = $vel_int->obtenerCarga();
$consulta = "INSERT INTO carga VALUES ("
    . "$carga[ancho_banda],"
    . "$carga[bytes],"
    . "$carga[transcurrido]"
    . ")";
$pdo->query("$consulta");

$descarga = $vel_int->obtenerDescarga();
$consulta = "INSERT INTO descarga VALUES ("
    . "$descarga[ancho_banda],"
    . "$descarga[bytes],"
    . "$descarga[transcurrido]"
    . ")";
$pdo->query("$consulta");

$interfaz = $vel_int->obtenerInterfaz();
// Guardar en la base de datos como cadena no como booleano
$interfaz['es_vpn'] = $interfaz['es_vpn'] ? 'true' : 'false';
$consulta = "INSERT INTO interfaz VALUES ("
    . "'$interfaz[es_vpn]',"
    . "'$interfaz[ip_externa]',"
    . "'$interfaz[ip_interna]',"
    . "'$interfaz[mac]',"
    . "'$interfaz[nombre]'"
    . ")";
$pdo->query("$consulta");

$medicion = $vel_int->obtenerMedicion();
$consulta = "INSERT INTO medicion VALUES ("
    . "'$medicion[fecha]',"
    . "'$medicion[isp]',"
    . "'$medicion[tipo]'"
    . ")";
$pdo->query("$consulta");

$ping = $vel_int->obtenerPing();
$consulta = "INSERT INTO ping VALUES ("
    . "$ping[fluctuacion],"
    . "$ping[latencia]"
    . ")";
$pdo->query("$consulta");

$resultado = $vel_int->obtenerResultado();
$consulta = "INSERT INTO resultado VALUES ("
    . "'$resultado[id]',"
    . "'$resultado[url]'"
    . ")";
$pdo->query("$consulta");

$servidor = $vel_int->obtenerServidor();
$consulta = "INSERT INTO servidor VALUES ("
    . "'$servidor[huesped]',"
    . "$servidor[id],"
    . "'$servidor[ip]',"
    . "'$servidor[nombre]',"
    . "'$servidor[pais]',"
    . "$servidor[puerto],"
    . "'$servidor[ubicacion]'"
    . ")";
$pdo->query("$consulta");
